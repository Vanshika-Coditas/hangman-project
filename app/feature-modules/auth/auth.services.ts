import { compare, genSalt, hash } from "bcryptjs";
import { AUTH_RESPONSES } from "./auth.responses";
import jwt, { sign, verify } from "jsonwebtoken";
import fs from "fs";
import path from "path";
import { IUser } from "../users/user.types";
import usersServices from "../users/users.services";
import { ICredentials, Payload } from "./auth.types";
import { Roles } from "../role/role.type";

const encryptUserPassword = async (user: IUser) => {
  const salt = await genSalt(10);
  const hashedPassword = await hash(user.password, salt);
  user.password = hashedPassword;

  return user;
};

const register = async (user: IUser) => {
  if (!user.role) {
    user.role = [Roles.VIEWVER];
  }
  user = await encryptUserPassword(user);
  const record = await usersServices.createUser(user);
  console.log(record, "in auth service");
  return record;
  console.log(record, "in auth service");
};

const login = async (credentials: ICredentials) => {
  const user = await usersServices.findOneUser({ email: credentials.email });

  if (!user) throw AUTH_RESPONSES.INVALID_CREDENTIALS;

  const isPasswordValid = await compare(credentials.password, user.password);

  if (!isPasswordValid) throw AUTH_RESPONSES.INVALID_CREDENTIALS;

  const { _id, role } = user;

  const PRIVATE_KEY = fs.readFileSync(
    path.resolve(__dirname, "..\\..\\keys\\private.pem"),
    { encoding: "utf-8" }
  );

  try {
    //payload, private key, algorithm
    var token = jwt.sign({ id: _id, role: role }, PRIVATE_KEY || "", {
      algorithm: "RS256",
      expiresIn: "8000s",
    });
    var refreshToken = jwt.sign({ id: _id, role: role }, PRIVATE_KEY || "", {
      algorithm: "RS256",
      expiresIn: "8000s",
    });
    return { token, refreshToken };
  } catch (error) {
    console.log(error);
  }
};

const refreshToken = (token: string) => {
  const PUBLIC_KEY = fs.readFileSync(
    path.resolve(__dirname, "..\\..\\keys\\public.pem"),
    { encoding: "utf-8" }
  );
  const tokenDecode = verify(token || "", PUBLIC_KEY || "") as Payload;
  console.log(tokenDecode);
  const PRIVATE_KEY = fs.readFileSync(
    path.resolve(__dirname, "..\\..\\keys\\private.pem"),
    { encoding: "utf-8" }
  );
  if (tokenDecode) {
    const accessToken = jwt.sign(
      { id: tokenDecode.id, role: tokenDecode.role },
      PRIVATE_KEY || "",
      { algorithm: "RS256", expiresIn: "900s" }
    );
    return { accessToken };
  } else {
    throw { statusCode: 400, message: "Token invalid" };
  }
};

// SYMMETRIC KEY
// const { JWT_SECRET } = process.env;

// const token = sign({id : _id, role : role}, JWT_SECRET || '')
// console.log(token)
// return { token };

const createAdmin = (user: IUser) => {
  user.role = [Roles.ADMIN as string];
  console.log(user, "in auth service");
  return register(user);
};

const createModerator = (user: IUser) => {
  user.role = [Roles.MODERATOR as string];
  return register(user);
};

export default {
  register,
  login,
  refreshToken,
  createAdmin,
  createModerator,
};
