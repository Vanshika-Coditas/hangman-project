import { body } from "express-validator";
import { validate } from "../../utility/validate";

export const LOGIN_VALIDATION = [
  body("email")
    .isString()
    .notEmpty()
    .withMessage("email is required")
    .withMessage("invalid email format"),
  body("password")
    .isString()
    .notEmpty()
    .withMessage("password is required")
    .isLength({ min: 6 })
    .withMessage("minimum 6 character required"),
  validate,
];

export const REGISTER_VALIDATION = [
  body("name").isString().notEmpty().withMessage("name is required"),
  body("email")
    .isString()
    .notEmpty()
    .withMessage("email is required")
    .isEmail()
    .withMessage("invalid email format"),
  validate,
];

export const ADMIN_VALIDATION = [
  body("name").isString().isLength({ min: 1 }).withMessage("Name Required!"),
  body("email").isEmail().withMessage("Invalid Email!"),
  body("password").isLength({ min: 5 }).withMessage("Invalid Password"),
  validate,
];

export const MODERATOR_VALIDATION = [
  body("name").isString().isLength({ min: 1 }).withMessage("Name Required!"),
  body("email").isEmail().withMessage("Invalid Email!"),
  body("password").isLength({ min: 5 }).withMessage("Invalid Password"),
  validate,
];
