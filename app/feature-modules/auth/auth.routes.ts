import Router, { Request,Response,NextFunction } from "express";
import { ResponseHandler } from "../../utility/response-handler";
import authServices from "./auth.services";
import { ADMIN_VALIDATION, LOGIN_VALIDATION, MODERATOR_VALIDATION, REGISTER_VALIDATION } from "./auth.validations";
import { permit } from "../../utility/middleware/permit";
import { Roles } from "../role/role.type";


const router = Router();

router.post("/register", REGISTER_VALIDATION,async(req : Request, res : Response, next : NextFunction) => {
    try{ 
        const user = req.body;
        const result = await authServices.register(user);
        res.send(new ResponseHandler(result));
    }catch(e){
        next(e)
    }
})

router.post("/login", LOGIN_VALIDATION, async(req : Request, res : Response, next : NextFunction)=>{

    try{ 
        const credentials = req.body;
        const result = await authServices.login(credentials);
        res.send(new ResponseHandler(result));
    }catch(e){
        next(e)
    }
})

router.post(
  "/refresh",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const result = await authServices.refreshToken(req.body.refreshToken);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.post(
  "/register-admin",
  permit([Roles.ADMIN]),
  ADMIN_VALIDATION,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.body;
      console.log(user)
      const result = await authServices.createAdmin(user);
      console.log(result,"in auth routes")
      res.send(new ResponseHandler(result));
    } catch (err) {
      next(err);
    }
  }
);

router.post(
  "/register-moderator",
  permit([Roles.ADMIN]),
  MODERATOR_VALIDATION,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.body;
      const result = await authServices.createModerator(user);
      res.send(new ResponseHandler(result));
    } catch (err) {
      next(err);
    }
  }
);

export default router;