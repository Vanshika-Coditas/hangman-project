import mongoose, { Schema } from "mongoose";

export interface IRole {
  _id: Schema.Types.ObjectId | string;
  name: string;
}
export const Roles = {
  ADMIN: "6453f6847d61fb5b5d8a5628",
  CREATER: "6453f6a57d61fb5b5d8a562a",
  MODERATOR: "6453f6c57d61fb5b5d8a562c",
  PLAYER: "64549b8d739677e160e603ab",
  VIEWVER: "6455fe13739677e160e603b2",
};
