import { body, param } from "express-validator";
import { validate } from "../../utility/validate";

export const CREATE_ROLE_VALIDATION = [
  body("name").isString().notEmpty().withMessage("name is required"),
];

export const UPDATE_ROLE_VALIDATION = [
  body("_id").isString().notEmpty().withMessage("_id is required"),
  body("name").optional().isString().notEmpty().withMessage("name is required"),
];

export const GET_GIFT_BY_ID_VALIDATION = [
  param("id").isString().notEmpty().withMessage("Role ID is required"),
  validate,
];