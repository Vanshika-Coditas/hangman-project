import { ResponseHandler } from "../../utility/response-handler";
import roleService from "./role.service";
import { Router } from "express";
import { NextFunction, Request, Response } from "express";
import { CREATE_ROLE_VALIDATION } from "./role.validations";

const router = Router();

router.post("/RegisterRole",CREATE_ROLE_VALIDATION,async (req:Request,res:Response,next:NextFunction)=>{
    try{
        const result = await roleService.create(req.body)
        res.send(new ResponseHandler(result))
    }
    catch(e){
        next(e)
    }
})

export default router;