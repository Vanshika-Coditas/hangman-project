import { Router } from "express";
import { ResponseHandler } from "../../utility/response-handler";
import { NextFunction, Request, Response } from "express";
import dashboardServices from "./dashboard.services"

const router=Router()
router.get(
  "/",
//   GET_NOTIFICATION_BY_ID_VALIDATION,
//   permit([Roles.ADMIN, Roles.PLAYER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const query = req.query;
      const result = await dashboardServices.findDashboard(query);
      res.json(result);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router;