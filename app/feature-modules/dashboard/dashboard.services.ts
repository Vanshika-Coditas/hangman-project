import { generatePipeline } from "../../utility/pipeline";
import leaderboardService from "../leaderBoard/leaderboard.service";
import tournamentService from "../tournaments/tournament.service";
import usersServices from "../users/users.services";

const findDashboard = async (query: Object) => {
  const pipeline = generatePipeline(query);
  const result1 = await leaderboardService.findallLeaderBoards(pipeline);
  const result2=await tournamentService.generateGraph();
  const result3 = await usersServices.findallUsers(pipeline);
  return {
    "LeaderBoards":result1,
    "graphDetails":result2,
    "userDetails":result3
  }
};
export default {
  findDashboard,
};
