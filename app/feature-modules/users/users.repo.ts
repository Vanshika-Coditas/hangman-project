import { FilterQuery, PipelineStage, Types, UpdateQuery } from "mongoose";
import { UserModel } from "./users.schema";
import { IUser } from "./user.types";

const findOneUser = (filters: Partial<IUser>) =>
  UserModel.findOne({
    ...filters,
    isDeleted: false,
  });

const createUser = async (user: IUser) => {
  const result = await UserModel.create(user);
  return result;
};

const findallUsers = async (pipeline: any) => {
  try {
    const result = await UserModel.aggregate(pipeline);
    return result;
  } catch (error) {
    console.error(error);
    throw new Error("Error finding gifts");
  }
};

const updateUser = async (
  filter: FilterQuery<IUser>,
  data: UpdateQuery<IUser>
) => {
  const result = await UserModel.updateOne(filter, data);
  console.log(result);
  return result;
};

const deleteOneUser = (id: string) =>
  UserModel.updateOne({ _id: new Types.ObjectId(id) }, { isDeleted: true });

const aggregation = (pipeline: PipelineStage[]) =>
  UserModel.aggregate(pipeline);

export default {
  aggregation,
  findOneUser,
  deleteOneUser,
  updateUser,
  findallUsers,
  createUser,
};
