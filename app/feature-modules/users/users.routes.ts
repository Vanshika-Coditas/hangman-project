import { NextFunction, Request, Response } from "express";
import { ResponseHandler } from "../../utility/response-handler";
import { Router } from "express";
const router = Router();

import mongoose, { Schema, Types } from "mongoose";
import {
  CREATE_USER_VALIDATION,
  GET_USER_BY_ID_VALIDATION,
  UPDATE_USER_VALIDATION,
} from "./users.validation";
import usersServices from "./users.services";
import { permit } from "../../utility/middleware/permit";
import { Roles } from "../role/role.type";

router.get("/findAllUsers", async (req, res, next) => {
  try {
    const query = req.query;
    const result = await usersServices.findallUsers(query);
    res.send(new ResponseHandler(result));
  } catch (e) {
    next(e);
  }
});

router.get(
  "/findOneUser/:id",
  GET_USER_BY_ID_VALIDATION,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const result = await usersServices.findOneUser({ _id: id });
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.patch(
  "/:userId",
  UPDATE_USER_VALIDATION,
  permit([Roles.ADMIN]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const userId = req.params.userId;
      const data = req.body;
      const result = await usersServices.updateUser(
        { _id: new Types.ObjectId(userId) },
        data
      );
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);


router.delete(
  "/deleteUser/:id",
  GET_USER_BY_ID_VALIDATION,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      console.log("shop routes deleted method");
      const { id } = req.params;
      const result = await usersServices.deleteUser(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router;
