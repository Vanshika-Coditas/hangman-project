import { model, Schema } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IUser } from "./user.types";

const userSchema = new BaseSchema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  username: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: [String],
    required: false,
  },
  rank: {
    type: Number,
    required: false,
  },
  points: {
    type: Number,
    required: false,
  },
  timeTaken: {
    type: Number,
    required: false,
  },
  noOfWins:{
    type:Number,
    required:false,
    default:0
  }
});

type UserDocument = Document & IUser;

export const UserModel = model<UserDocument>("users", userSchema);
