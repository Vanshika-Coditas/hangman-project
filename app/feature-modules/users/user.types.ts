import { Schema } from "mongoose";

export interface IUser {
  _id: string | Schema.Types.ObjectId;
  name: string;
  username: string;
  password: string;
  email: string;
  role?: [string];
  rank?: number;
  timeTaken?: number;
  points?: number;
  noOfWins: number;
}

export type Users = IUser[];
