
import { IUser } from "./user.types";
import usersRepo from "./users.repo";
import { USER_RESPONSE } from "./users.response";
import { generatePipeline } from "../../utility/pipeline";
import { FilterQuery, PipelineStage, UpdateQuery } from "mongoose";

const findOneUser = async (filter: Partial<IUser>) => {
  const user = await usersRepo.findOneUser(filter);
  if (!user) throw USER_RESPONSE.NOT_FOUND;

  return user;
};

const updateUser = async (
  filter: FilterQuery<IUser>,
  data: UpdateQuery<IUser>
) => {
  const result = await usersRepo.updateUser(filter, data);
  if (result.modifiedCount > 0) return USER_RESPONSE.UPDATE_SUCCESS;
  return USER_RESPONSE.NOT_FOUND;
};

const deleteUser = async (id: string) =>{
  await usersRepo.deleteOneUser(id);
}

const createUser = async (user: IUser) => {
  const result = await usersRepo.createUser(user);
  return result;
};

const findallUsers = async (query: Object) => {
  const pipeline = generatePipeline(query);
  const result = await usersRepo.findallUsers(pipeline);
  return result;
};

const aggregation = (pipeline: PipelineStage[]) =>
  usersRepo.aggregation(pipeline);

export default {
  aggregation,
  createUser,
  findallUsers,
  findOneUser,
  updateUser,
  deleteUser,
};
