export const USER_RESPONSE = {
  NOT_FOUND: {
    statusCode: 404,
    message: "user not found",
  },
  UPDATE_SUCCESS: {
    statusCode: 200,
    message: "user updated",
  },
};
