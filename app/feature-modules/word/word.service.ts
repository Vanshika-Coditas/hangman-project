import { FilterQuery, PipelineStage, Types, UpdateQuery } from "mongoose";
import { generatePipeline } from "../../utility/pipeline";
import { IWord } from "./word.types";
import { WORD_RESPONSE } from "./word.response";
import wordRepo from "./word.repo";

const createWord = (word: IWord) => wordRepo.createWord(word);

const updateWord = async (
  filter: FilterQuery<IWord>,
  data: UpdateQuery<IWord>
) => {
  const result = await wordRepo.updateWord(filter, data);
  if (result.modifiedCount > 0) return WORD_RESPONSE.UPDATE_SUCCESS;
  return WORD_RESPONSE.NOT_FOUND;
};

const deleteWord = async (id: string) => await wordRepo.deleteWord(id);

const findOneWord = async (filter: FilterQuery<IWord>) =>
  wordRepo.findOneWord(filter);

const findallWords = async (query: Object) => {
  const pipeline = generatePipeline(query);
  const result = await wordRepo.findallWords(pipeline);
  return result;
};
const aggregation = (pipeline: PipelineStage[]) =>
  wordRepo.aggregation(pipeline);

const approveWords=async(wordId:string)=>{
  return await wordRepo.updateWord({ _id: wordId },{isApproved:true});
}

export default {
  approveWords,
  findallWords,
  findOneWord,
  deleteWord,
  updateWord,
  createWord,
  aggregation
};
