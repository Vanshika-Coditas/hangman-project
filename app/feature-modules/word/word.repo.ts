import { FilterQuery, PipelineStage, Types, UpdateQuery } from "mongoose";
import { IWord } from "./word.types";
import { wordModel } from "./word.schema";

const createWord = (word: IWord) => wordModel.create(word);

const updateWord = async (
  filter: FilterQuery<IWord>,
  data: UpdateQuery<IWord>
) => {
  const result = await wordModel.updateOne(filter, data);
  console.log(result);
  return result;
};

const deleteWord = async (id: string) =>
  await wordModel.updateOne(
    { _id: new Types.ObjectId(id) },
    { isDeleted: true }
  );

const findallWords = async (pipeline: any) => {
  try {
    const result = await wordModel.aggregate(pipeline);
    return result;
  } catch (error) {
    console.error(error);
    throw new Error("Error finding gifts");
  }
};

const findOneWord = async (filter: FilterQuery<IWord>) =>
  wordModel.find(filter);

const aggregation = (pipeline: PipelineStage[]) =>
  wordModel.aggregate(pipeline);

export default {
  findOneWord,
  findallWords,
  deleteWord,
  updateWord,
  createWord,
  aggregation
};
