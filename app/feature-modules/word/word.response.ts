export const WORD_RESPONSE = {
  NOT_FOUND: {
    statusCode: 404,
    message: "word not found",
  },
  UNABLE_TO_PROCEED: {
    statusCode: 400,
    message: "unable to  proceed",
  },
  UPDATE_SUCCESS: {
    message: "word Updated Successfully",
    statusCode: 200,
  },
};
