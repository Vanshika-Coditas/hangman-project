import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IWord } from "./word.types";

const wordSchema = new BaseSchema({
  wordName: {
    type: String,
    required: true,
  },
  category: {
    type: [Schema.Types.ObjectId],
    required: true,
    ref: "category",
  },
  createrId: {
    type: Schema.Types.ObjectId,
    required: true,
  },

  level: {
    type: String,
    required: true,
  },
});

type wordDocument = Document & IWord;

export const wordModel = model<wordDocument>("word", wordSchema);
