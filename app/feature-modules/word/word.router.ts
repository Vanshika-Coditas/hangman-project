import { Router } from "express";
import { ResponseHandler } from "../../utility/response-handler";
import { NextFunction, Request, Response } from "express";
import { permit } from "../../utility/middleware/permit";
import categoryService from "./word.service";
import {
  createWordValidation,
  updateWordValidation,
  getWordByIdValidation,
} from "./word.validations";
import { Roles } from "../role/role.type";
import { Types } from "mongoose";
import wordService from "./word.service";

const router = Router();

router.post(
  "/",
  createWordValidation,
  permit([Roles.ADMIN,Roles.CREATER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const word = req.body;
      const result = await wordService.createWord(word);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.patch(
  "/:wordId",
  updateWordValidation,
  permit([Roles.ADMIN]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const wordId = req.params.wordId;
      const data = req.body;
      const result = await wordService.updateWord(
        { _id: new Types.ObjectId(wordId) },
        data
      );
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.patch(
  "/:wordId",
  updateWordValidation,
  permit([Roles.ADMIN]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const wordId = req.params.wordId;
      const data = req.body;
      const result = await wordService.updateWord(
        { _id: new Types.ObjectId(wordId) },
        data
      );
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.delete(
  "/:id",
  getWordByIdValidation,
  permit([Roles.ADMIN]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      console.log("shop routes deleted method");
      const { id } = req.params;
      const result = await wordService.deleteWord(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get(
  "/",
  permit([Roles.ADMIN, Roles.PLAYER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const query = req.query;
      const result = await wordService.findallWords(query);
      res.json(result);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get(
  "/findOneWord/:id",
  getWordByIdValidation,
  permit([Roles.ADMIN, Roles.PLAYER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const result = await categoryService.findOneWord({ _id: id });
      console.log("result");
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router;
