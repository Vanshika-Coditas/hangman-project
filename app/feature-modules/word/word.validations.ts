import { body, param } from "express-validator";

export const createWordValidation = [
  body("wordName").isString().notEmpty().withMessage("Word name is required"),
  body("category").isArray().notEmpty().withMessage("Category is required"),
  body("level").isNumeric().notEmpty().withMessage("Level is required"),
];

export const updateWordValidation = [
  param("id").isString().notEmpty().withMessage("Word ID is required"),
  body("wordName")
    .optional()
    .isString()
    .notEmpty()
    .withMessage("Word name is required"),
  body("category")
    .optional()
    .isArray()
    .notEmpty()
    .withMessage("Category is required"),
  body("level")
    .optional()
    .isNumeric()
    .notEmpty()
    .withMessage("Level is required"),
];

export const getWordByIdValidation = [
  param("id").isString().notEmpty().withMessage("Word ID is required"),
];
