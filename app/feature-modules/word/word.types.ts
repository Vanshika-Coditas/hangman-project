import { Schema } from "mongoose";

export interface IWord {
  _id?: string | Schema.Types.ObjectId;
  wordName: string;
  category: [string];
  level: string;
  createrId:string;
}



