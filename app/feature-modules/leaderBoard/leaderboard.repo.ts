import { FilterQuery, PipelineStage, Types, UpdateQuery } from "mongoose";
import { ILeaderboard } from "./leaderBoard.types";
import { leaderboardModel } from "./leaderboard.schema";

const createLeaderBoard = (board: ILeaderboard) =>
  leaderboardModel.create(board);

const updateLeaderBoard = async (
  filter: FilterQuery<ILeaderboard>,
  data: UpdateQuery<ILeaderboard>
) => {
  const result = await leaderboardModel.updateOne(filter, data);
  console.log(result);
  return result;
};

// const deleteCategory = async (id: string) =>
//   await categoryModel.updateOne(
//     { _id: new Types.ObjectId(id) },
//     { isDeleted: true }
//   );

const findallLeaderBoards = async (pipeline: any) => {
  try {
    const result = await leaderboardModel.aggregate(pipeline);
    return result;
  } catch (error) {
    console.error(error);
    throw new Error("Error finding leaderboard");
  }
};

const findOneLeaderBoard = async (filter: FilterQuery<ILeaderboard>) =>
  leaderboardModel.find(filter);

const aggregation = (pipeline: PipelineStage[]) =>
  leaderboardModel.aggregate(pipeline);

const findLeaderBoardPlayer = (filter: any) => {
    leaderboardModel.aggregate([
      {
        $match: {
          tourID: filter.tourID,
        },
      },
      {
        $unwind: "$playerInfo",
      },
      {
        $match: {
          "playerInfo.playerId": filter.playerId,
        },
      },
    ]);
};

export default {
  aggregation,
  findOneLeaderBoard,
  findallLeaderBoards,
  updateLeaderBoard,
  createLeaderBoard,
  findLeaderBoardPlayer
};
