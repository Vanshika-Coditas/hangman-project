import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { ILeaderboard } from "./leaderBoard.types";

const leaderboardSchema = new BaseSchema({
  tourID: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: "tournaments",
  },
  playerInfo: [
    {
      playerId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "users",
      },
      noOfMistakes: {
        type: Number,
        required: true,
      },
      points: {
        type: Number,
        required: true,
      },
      timeInSeconds: {
        type: Number,
        required: true,
      },
    },
  ],
});

type LeaderBoardDocument = Document & ILeaderboard;

export const leaderboardModel = model<LeaderBoardDocument>(
  "leaderboard",
  leaderboardSchema
);
