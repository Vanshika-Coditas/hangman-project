import { Schema } from "mongoose";

export interface ILeaderboard {
  tourID: string;
  playerInfo: [playerInfo];
}
export interface playerInfo{
  playerId:string;
  noOfMistakes:number;
  timeInSeconds:number;
  points:number
}


