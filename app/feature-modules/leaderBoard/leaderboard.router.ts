import { Router } from "express";
import { ResponseHandler } from "../../utility/response-handler";
import { NextFunction, Request, Response } from "express";
import { permit } from "../../utility/middleware/permit";
// import {
//   createCategoryValidation,
//   updateCategoryValidation,
//   getCategoryValidation,
// } from "./category.validations";
import { Roles } from "../role/role.type";
import { Types } from "mongoose";
import tournamentService from "../tournaments/tournament.service";
import leaderboardService from "./leaderboard.service";

const router = Router();

router.post(
  "/",
//   createCategoryValidation,
//   permit([Roles.ADMIN]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const board = req.body;
      console.log(board);
      const result = await leaderboardService.createLeaderBoard(board);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.patch(
  "/:boardId",
//   updateCategoryValidation,
//   permit([Roles.ADMIN]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const boardId = req.params.boardId;
      const data = req.body;
      const result = await leaderboardService.updateLeaderBoard(
        { _id: new Types.ObjectId(boardId) },
        data
      );
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

// router.delete(
//   "/:id",
//   getCategoryValidation,
//   permit([Roles.ADMIN]),
//   async (req: Request, res: Response, next: NextFunction) => {
//     try {
//       console.log("shop routes deleted method");
//       const { id } = req.params;
//       const result = await categoryService.deleteOneCategory(id);
//       res.send(new ResponseHandler(result));
//     } catch (e) {
//       next(e);
//     }
//   }
// );

// router.get(
//   "/",
//   getCategoryValidation,
//   permit([Roles.ADMIN, Roles.PLAYER]),
//   async (req:Request, res:Response, next:NextFunction) => {
//     try {
//       const query = req.query;
//       const result = await categoryService.findallCategory(query);
//       res.json(result);
//       res.send(new ResponseHandler(result));
//     } catch (e) {
//       next(e);
//     }
//   }
// );

// router.get(
//   "/findOneCategory/:id",
//   getCategoryValidation,
//   permit([Roles.ADMIN, Roles.PLAYER]),
//   async (req: Request, res: Response, next: NextFunction) => {
//     try {
//       const { id } = req.params;
//       const result = await categoryService.findOneCategory({ _id: id });
//       console.log("result");
//       res.send(new ResponseHandler(result));
//     } catch (e) {
//       next(e);
//     }
//   }
// );

export default router;
