import { FilterQuery, PipelineStage, Types, UpdateQuery } from "mongoose";
import { generatePipeline } from "../../utility/pipeline";
import { ILeaderboard } from "./leaderBoard.types";
import leaderboardRepo from "./leaderboard.repo";
import { LEADERBOARD_RESPONSE } from "./leaderboard.response";


const createLeaderBoard = (board: ILeaderboard) => leaderboardRepo.createLeaderBoard(board);

const updateLeaderBoard = async (
  filter: FilterQuery<ILeaderboard>,
  data: UpdateQuery<ILeaderboard>
) => {
  const result = await leaderboardRepo.updateLeaderBoard(filter, data);
  if (result.modifiedCount > 0) return LEADERBOARD_RESPONSE.UPDATE_SUCCESS;
  return LEADERBOARD_RESPONSE.NOT_FOUND;
};

// const deleteOneCategory = async (id: string) =>
//   await categoryRepo.deleteCategory(id);

const findOneLeaderBoard = async (filter: FilterQuery<ILeaderboard>) =>
  leaderboardRepo.findOneLeaderBoard(filter);

const findLeaderBoardPlayer = async (filter: FilterQuery<ILeaderboard>) => {
  return leaderboardRepo.findLeaderBoardPlayer(filter);
};

const findallLeaderBoards = async (query: Object) => {
  const pipeline = generatePipeline(query);
  const result = await leaderboardRepo.findallLeaderBoards(pipeline);
  return result;
};

const aggregation = (pipeline: PipelineStage[]) =>
  leaderboardRepo.aggregation(pipeline);

export default {
  aggregation,
  findallLeaderBoards,
  findOneLeaderBoard,
  updateLeaderBoard,
  createLeaderBoard,
  findLeaderBoardPlayer,
};
