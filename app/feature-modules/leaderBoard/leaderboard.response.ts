export const LEADERBOARD_RESPONSE = {
  NOT_FOUND: {
    statusCode: 404,
    message: "leaderboard not found",
  },
  UNABLE_TO_PROCEED: {
    statusCode: 400,
    message: "unable to  proceed",
  },
  UPDATE_SUCCESS: {
    message: "leaderboard Updated Successfully",
    statusCode: 200,
  },
};
