import { FilterQuery, Types, UpdateQuery } from "mongoose";
import { INotification } from "./notification.types";
import { NotificationModel } from "./notification.schema";


const createNotification = (not: INotification) => NotificationModel.create(not);

// const updateCategory = async (
//   filter: FilterQuery<ICategory>,
//   data: UpdateQuery<ICategory>
// ) => {
//   const result = await categoryModel.updateOne(filter, data);
//   console.log(result);
//   return result;
// };

// const deleteCategory = async (id: string) =>
//   await categoryModel.updateOne(
//     { _id: new Types.ObjectId(id) },
//     { isDeleted: true }
//   );

const findallNotifications = async (pipeline: any) => {
  try {
    const result = await NotificationModel.aggregate(pipeline);
    return result;
  } catch (error) {
    console.error(error);
    throw new Error("Error finding gifts");
  }
};

const findOneNotification = async (filter: FilterQuery<INotification>) =>
  NotificationModel.find(filter);

export default {
  createNotification,
  findOneNotification,
  findallNotifications
};
