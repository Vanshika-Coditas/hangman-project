import { FilterQuery, Types, UpdateQuery } from "mongoose";
import { generatePipeline } from "../../utility/pipeline";
import { INotification } from "./notification.types";
import notificationRepo from "./notification.repo";



const createNotification = (not: INotification) => notificationRepo.createNotification(not);

// const updateCategory = async (
//   filter: FilterQuery<ICategory>,
//   data: UpdateQuery<ICategory>
// ) => {
//   const result = await categoryRepo.updateCategory(filter, data);
//   if (result.modifiedCount > 0) return CATEGORY_RESPONSE.UPDATE_SUCCESS;
//   return CATEGORY_RESPONSE.NOT_FOUND;
// };

// const deleteOneCategory = async (id: string) =>
//   await categoryRepo.deleteCategory(id);

const findOneNotification = async (filter: FilterQuery<INotification>) =>
  notificationRepo.findOneNotification(filter);

const findallNotifications = async (query: Object) => {
  const pipeline = generatePipeline(query);
  const result = await notificationRepo.findallNotifications(pipeline);
  return result;
};

export default {
  findallNotifications,
  createNotification,
  findOneNotification
};
