import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { INotification } from "./notification.types";

const NotificationSchema = new BaseSchema({
  senderId: {
    type: Schema.Types.ObjectId,
    required: true,
    Ref: "users",
  },
  receiverId: {
    type: Schema.Types.ObjectId,
    required: true,
    Ref: "users",
  },
  Notmessage: {
    type: String,
    required: false,
    default: "you have a notification",
  },
  tournamentId: {
    type: String,
    require: false,
  },
});

type NotDocument = Document & INotification; //shape

export const NotificationModel = model<NotDocument>(
  "Notification",
  NotificationSchema
);
