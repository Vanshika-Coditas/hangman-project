import { body, param } from "express-validator";
import { validate } from "../../utility/validate";

export const CREATE_NOTIFICATION_VALIDATION = [
  body("userId").isString().notEmpty().withMessage("userId is required"),
  body("receiverId")
    .isString()
    .notEmpty()
    .withMessage("receiverId is required"),
  body("message").isString().notEmpty().withMessage("message is required"),
  validate,
];
export const GET_NOTIFICATION_BY_ID_VALIDATION = [
  param("id").isString().notEmpty().withMessage("Notification ID is required"),
  validate,
];
