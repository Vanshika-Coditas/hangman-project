import { Router } from "express";
import { ResponseHandler } from "../../utility/response-handler";
import { NextFunction, Request, Response } from "express";
import { permit } from "../../utility/middleware/permit";
import { Roles } from "../role/role.type";
import { Types } from "mongoose";
import { CREATE_NOTIFICATION_VALIDATION, GET_NOTIFICATION_BY_ID_VALIDATION } from "./notification.validation";
import notificationService from "./notification.service";

const router = Router();

router.post(
  "/",
  CREATE_NOTIFICATION_VALIDATION,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const notData = req.body;
      console.log(notData);
      const result = await notificationService.createNotification(notData);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

// router.patch(
//   "/:shopId",
//   updateCategoryValidation,
//   permit([Roles.ADMIN]),
//   async (req: Request, res: Response, next: NextFunction) => {
//     try {
//       const shopId = req.params.shopId;
//       const data = req.body;
//       const result = await categoryService.updateCategory(
//         { _id: new Types.ObjectId(shopId) },
//         data
//       );
//       res.send(new ResponseHandler(result));
//     } catch (e) {
//       next(e);
//     }
//   }
// );

// router.delete(
//   "/:id",
//   getCategoryValidation,
//   permit([Roles.ADMIN]),
//   async (req: Request, res: Response, next: NextFunction) => {
//     try {
//       console.log("shop routes deleted method");
//       const { id } = req.params;
//       const result = await categoryService.deleteOneCategory(id);
//       res.send(new ResponseHandler(result));
//     } catch (e) {
//       next(e);
//     }
//   }
// );

router.get(
  "/",
  GET_NOTIFICATION_BY_ID_VALIDATION,
  permit([Roles.ADMIN, Roles.PLAYER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const query = req.query;
      const result = await notificationService.findallNotifications(query);
      res.json(result);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get(
  "/findOneCategory/:id",
  GET_NOTIFICATION_BY_ID_VALIDATION,
  permit([Roles.ADMIN, Roles.PLAYER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const result = await notificationService.findOneNotification({ _id: id });
      console.log("result");
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router;
