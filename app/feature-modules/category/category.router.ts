import { Router } from "express";
import { ResponseHandler } from "../../utility/response-handler";
import { NextFunction, Request, Response } from "express";
import { permit } from "../../utility/middleware/permit";
import categoryService from "./category.service";
import {
  createCategoryValidation,
  updateCategoryValidation,
  getCategoryValidation,
} from "./category.validations";
import { Roles } from "../role/role.type";
import { Types } from "mongoose";

const router = Router();

router.post(
  "/",
  createCategoryValidation,
  permit([Roles.ADMIN]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const catData = req.body;
      console.log(catData)
      const result = await categoryService.createCategory(catData);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.patch(
  "/:shopId",
  updateCategoryValidation,
  permit([Roles.ADMIN]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const shopId = req.params.shopId;
      const data = req.body;
      const result = await categoryService.updateCategory(
        { _id: new Types.ObjectId(shopId) },
        data
      );
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.delete(
  "/:id",
  getCategoryValidation,
  permit([Roles.ADMIN]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      console.log("shop routes deleted method");
      const { id } = req.params;
      const result = await categoryService.deleteOneCategory(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get(
  "/",
  getCategoryValidation,
  permit([Roles.ADMIN, Roles.PLAYER]),
  async (req:Request, res:Response, next:NextFunction) => {
    try {
      const query = req.query;
      const result = await categoryService.findallCategory(query);
      res.json(result);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get(
  "/findOneCategory/:id",
  getCategoryValidation,
  permit([Roles.ADMIN, Roles.PLAYER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const result = await categoryService.findOneCategory({ _id: id });
      console.log("result");
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router;
