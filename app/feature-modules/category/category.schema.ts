import { model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { ICategory } from "./category.types";

const categorySchema = new BaseSchema({
  categoryName: {
    type: String,
    required: true,
  }
});

type categotyDocument = Document & ICategory;

export const categoryModel = model<categotyDocument>("category", categorySchema);
