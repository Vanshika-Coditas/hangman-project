import { FilterQuery, Types, UpdateQuery } from "mongoose";
import { generatePipeline } from "../../utility/pipeline";
import giftsRepo from "./category.repo";
import { ICategory } from "./category.types";
import categoryRepo from "./category.repo";
import { CATEGORY_RESPONSE } from "./category.response";

const createCategory = (cat: ICategory) => categoryRepo.createCategory(cat);

const updateCategory = async (
  filter: FilterQuery<ICategory>,
  data: UpdateQuery<ICategory>
) => {
  const result = await categoryRepo.updateCategory(filter, data);
  if (result.modifiedCount > 0) return CATEGORY_RESPONSE.UPDATE_SUCCESS;
  return CATEGORY_RESPONSE.NOT_FOUND;
};

const deleteOneCategory = async (id: string) =>
  await categoryRepo.deleteCategory(id);

const findOneCategory = async (filter: FilterQuery<ICategory>) =>
  giftsRepo.findOneCategory(filter);

const findallCategory = async (query: Object) => {
  const pipeline = generatePipeline(query);
  const result = await giftsRepo.findallCategory(pipeline);
  return result;
};

export default {
  createCategory,
  updateCategory,
  deleteOneCategory,
  findOneCategory,
  findallCategory,
};
