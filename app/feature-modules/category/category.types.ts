import { Schema } from "mongoose";

export interface ICategory {
  _id?: string | Schema.Types.ObjectId;
  categoryName: string;
}



