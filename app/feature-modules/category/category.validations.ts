import { body, param } from "express-validator";

export const createCategoryValidation = [
  body("categoryName").isString().withMessage("Category name must be a string"),
  body("_id").optional().isMongoId().withMessage("Invalid ID format"),
];

export const updateCategoryValidation = [
  param("id").isMongoId().withMessage("Invalid ID format"),
  body("categoryName")
    .optional()
    .isString()
    .withMessage("Category name must be a string"),
  body("_id").optional().isMongoId().withMessage("Invalid ID format"),
];

export const getCategoryValidation = [
  param("id").isMongoId().withMessage("Invalid ID format"),
];
