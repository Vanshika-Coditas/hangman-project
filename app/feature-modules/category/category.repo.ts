import { FilterQuery, Types, UpdateQuery } from "mongoose";
import { ICategory } from "./category.types";
import { categoryModel } from "./category.schema";

const createCategory = (cat: ICategory) => categoryModel.create(cat);

const updateCategory = async (
  filter: FilterQuery<ICategory>,
  data: UpdateQuery<ICategory>
) => {
  const result = await categoryModel.updateOne(filter, data);
  console.log(result);
  return result;
};

const deleteCategory = async (id: string) =>
  await categoryModel.updateOne(
    { _id: new Types.ObjectId(id) },
    { isDeleted: true }
  );

const findallCategory = async (pipeline: any) => {
  try {
    const result = await categoryModel.aggregate(pipeline);
    return result;
  } catch (error) {
    console.error(error);
    throw new Error("Error finding gifts");
  }
};

const findOneCategory = async (filter: FilterQuery<ICategory>) =>
  categoryModel.find(filter);

export default {
  createCategory,
  updateCategory,
  deleteCategory,
  findallCategory,
  findOneCategory
};
