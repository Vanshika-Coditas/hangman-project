export const CATEGORY_RESPONSE = {
  NOT_FOUND: {
    statusCode: 404,
    message: "category not found",
  },
  UNABLE_TO_PROCEED: {
    statusCode: 400,
    message: "unable to  proceed",
  },
  UPDATE_SUCCESS: {
    message: "category Updated Successfully",
    statusCode: 200,
  },
};
