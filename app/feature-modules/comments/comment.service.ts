import {
  FilterQuery,
  PipelineStage,
  Schema,
  UpdateQuery,
} from "mongoose";
import { Types } from "mongoose";
import { generatePipeline } from "../../utility/pipeline";
import { IComment } from "./comment.types";
import commentRepo from "./comment.repo";
import { COMMENT_RESPONSE } from "./comment.response";
import { Roles } from "../role/role.type";
import { INotification } from "../notification/notification.types";
import notificationService from "../notification/notification.service";

const createComment = (comment: IComment) => commentRepo.createComment(comment);

const updateComment = async (
  filter: FilterQuery<IComment>,
  data: UpdateQuery<IComment>
) => {
  const result = await commentRepo.updateComment(filter, data);
  if (result.modifiedCount > 0) return COMMENT_RESPONSE.UPDATE_SUCCESS;
  return COMMENT_RESPONSE.NOT_FOUND;
};

const deleteOneComment = async (id: string) =>
  await commentRepo.deleteComment(id);

const findOneComment = async (filter: FilterQuery<IComment>) =>
  commentRepo.findOneComment(filter);

const findallcomments = async (query: Object) => {
  const pipeline = generatePipeline(query);
  const result = await commentRepo.findallcomments(pipeline);
  return result;
};

const aggregation = (pipeline: PipelineStage[]) =>
  commentRepo.aggregation(pipeline);

const warnComments = async (commentId: string, reason: string) => {
  const comment = await findOneComment({
    _id: new Types.ObjectId(commentId),
    type:"type1"
  });
  console.log(comment, "comment");
  const tournamentId = comment[0].tournamentId;
  const receiverId = comment[0].userId;
  const senderId = Roles.MODERATOR;
  ///create  a notificatoin

  const notification: INotification = {
    senderId: senderId,
    receiverId: receiverId,
    tournamentId: tournamentId,
    Notmessge: `Your comment has been flagged for the following reason: ${reason}`,
  };
  // Create notification
  const createdNotification = await notificationService.createNotification(
    notification
  );
  console.log(createdNotification, "createdNotification");
  const warnedComment = await updateComment(
    {
      _id: new Types.ObjectId(commentId),
    },
    { warningFlag: true, warnReason: reason }
  );
  console.log(warnedComment, "warnedComment");
  //delete the comment which is not appropriate
  return await deleteOneComment(commentId);
};

export default {
  findallcomments,
  findOneComment,
  deleteOneComment,
  updateComment,
  createComment,
  aggregation,
  warnComments,
};
