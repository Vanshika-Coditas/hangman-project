export const COMMENT_RESPONSE = {
  NOT_FOUND: {
    statusCode: 404,
    message: "comment not found",
  },
  UNABLE_TO_PROCEED: {
    statusCode: 400,
    message: "unable to  proceed",
  },
  UPDATE_SUCCESS: {
    message: "comment Updated Successfully",
    statusCode: 200,
  },
};
