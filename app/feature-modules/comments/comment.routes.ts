import { Router } from "express";
import { ResponseHandler } from "../../utility/response-handler";
import { NextFunction, Request, Response } from "express";
import { permit } from "../../utility/middleware/permit";
import { Roles } from "../role/role.type";
import { Types } from "mongoose";
import {
  CREATE_COMMENT_VALIDATION,
  UPDATE_COMMENT_VALIDATION,
  GET_COMMENT_BY_ID_VALIDATION,
} from "./comment.validation";
import notificationService from "./comment.service";
import commentService from "./comment.service";

const router = Router();

router.post(
  "/",
  CREATE_COMMENT_VALIDATION,
  // permit([Roles.VIEWVER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const commentData = req.body;
      console.log(commentData);
      const result = await commentService.createComment(commentData);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.patch(
  "/:commentId",
  UPDATE_COMMENT_VALIDATION,
  permit([Roles.VIEWVER, Roles.MODERATOR]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const commentId = req.params.commentId;
      const data = req.body;
      const result = await commentService.updateComment(
        { _id: new Types.ObjectId(commentId) },
        data
      );
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.delete(
  "/:id",
  GET_COMMENT_BY_ID_VALIDATION,
  permit([Roles.MODERATOR]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      console.log("shop routes deleted method");
      const { id } = req.params;
      const result = await commentService.deleteOneComment(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get(
  "/",
  GET_COMMENT_BY_ID_VALIDATION,
  permit([Roles.ADMIN, Roles.MODERATOR]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const query = req.query;
      const result = await commentService.findallcomments(query);
      res.json(result);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.post(
  "/comments/:id/warn",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      // Extract comment ID and reason from request body
      const commentId: string = req.params.id;
      const reason: string = req.body.reason;

      // Call the warnComments function
      const result = await commentService.warnComments(commentId, reason);

      // Send a response with the deleted comment object
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get(
  "/findOneComment/:id",
  GET_COMMENT_BY_ID_VALIDATION,
  permit([Roles.ADMIN, Roles.MODERATOR]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const result = await commentService.findOneComment({ _id: id });
      console.log("result");
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router;
