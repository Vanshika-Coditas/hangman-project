import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IComment } from "./comment.types";

const CommentSchema = new BaseSchema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
    Ref: "users",
  },
  tournamentId: {
    type: Schema.Types.ObjectId,
    required: true,
    Ref: "tournament",
  },
  commentMessege: {
    type: [String],
    required: true,
  },
  moderatorId: {
    type: Schema.Types.ObjectId,
    required: true,
    Ref: "users",
  },
  warnReason: {
    type: String,
    required: true,
    default: "your comment was inappropriate",
  },
  warningFlag: {
    type: Boolean,
    required: false,
    default: false,
  },
  type: {
    type: String,
    required: true,
  },
});

type CommentDocument = Document & IComment; //shape

export const CommentModel = model<CommentDocument>("Comment", CommentSchema);
