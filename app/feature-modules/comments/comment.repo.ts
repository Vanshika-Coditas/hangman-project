import { FilterQuery, PipelineStage, Types, UpdateQuery } from "mongoose";
import { IComment } from "./comment.types";
import { CommentModel } from "./comment.schema";

const createComment = (comment: IComment) => CommentModel.create(comment);

const updateComment = async (
  filter: FilterQuery<IComment>,
  data: UpdateQuery<IComment>
) => {
  const result = await CommentModel.updateOne(filter, data);
  console.log(result);
  return result;
};

const deleteComment = async (id: string) =>
  await CommentModel.updateOne(
    { _id: new Types.ObjectId(id) },
    { isDeleted: true }
  );

const findallcomments = async (pipeline: any) => {
  try {
    const result = await CommentModel.aggregate(pipeline);
    return result;
  } catch (error) {
    console.error(error);
    throw new Error("Error finding gifts");
  }
};

const findOneComment = async (filter: FilterQuery<IComment>) =>
  CommentModel.find(filter);

const aggregation = (pipeline: PipelineStage[]) =>
  CommentModel.aggregate(pipeline);



export default {
  findOneComment,
  findallcomments,
  deleteComment,
  updateComment,
  createComment,
  aggregation,
};
