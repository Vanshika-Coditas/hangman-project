export interface IComment{
    userId:string,
    tournamentId:string,
    commentMessege:[string],
    warningFlag?:Boolean,
    warnReason?:string,
    moderatorId?:string
    type:string
}