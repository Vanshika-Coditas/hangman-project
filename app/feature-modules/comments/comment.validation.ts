
import { body, param } from 'express-validator';
import { validate } from '../../utility/validate';

export const CREATE_COMMENT_VALIDATION = [
  body('userId').isString().notEmpty().withMessage('User ID is required'),
  body('tournamentId').isString().notEmpty().withMessage('Tournament ID is required'),
  body('commentMessege').isString().notEmpty().withMessage('Comment message is required'),
  body('warningFlag').optional().isBoolean().withMessage('Warning flag must be a boolean value'),
  body('warnReason').optional().isString().withMessage('Warn reason must be a string'),
  body('moderatorId').optional().isString().withMessage('Moderator ID must be a string'),
  validate,
];

export const UPDATE_COMMENT_VALIDATION = [
  body("_id").isString().notEmpty().withMessage("_id is required"),
  body("userId").optional().isString().withMessage("User ID must be a string"),
  body("tournamentId")
    .optional()
    .isString()
    .withMessage("Tournament ID must be a string"),
  body("commentMessege")
    .optional()
    .isString()
    .withMessage("Comment message must be a string"),
  body("warningFlag")
    .optional()
    .isBoolean()
    .withMessage("Warning flag must be a boolean value"),
  body("warnReason")
    .optional()
    .isString()
    .withMessage("Warn reason must be a string"),
  body("moderatorId")
    .optional()
    .isString()
    .withMessage("Moderator ID must be a string"),
  validate,
];

export const GET_COMMENT_BY_ID_VALIDATION = [
  param("id").isString().notEmpty().withMessage("Comment ID is required"),
  validate,
];