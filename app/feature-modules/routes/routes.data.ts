import { Route, Routes } from "./routes.types";
import Routers from "../../feature-modules/index";
import {
  ExcludedPath,
  ExcludedPaths,
} from "../../utility/middleware/middleware";
export const routes: Routes = [
  new Route("/auth", Routers.authRouter),
  new Route("/users", Routers.userRouter),
  new Route("/role", Routers.RoleRouter),
  new Route("/category", Routers.categoryRouter),
  new Route("/word", Routers.wordRouter),
  new Route("/notification", Routers.notificationRouter),
  new Route("/comments", Routers.commentRouter),
  new Route("/tournaments", Routers.tournamentRouter),
  new Route("/leaderboard", Routers.leaderboardRouter),
  new Route("/dashboard", Routers.dashboardRouter),
];

export const excludedPaths: ExcludedPaths = [
  new ExcludedPath("/auth/Register", "POST"),
  new ExcludedPath("/auth/Login", "POST"),
  new ExcludedPath("/auth/refresh", "POST"),
];
