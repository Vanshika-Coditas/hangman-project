import authRouter from "./auth/auth.routes";
import userRouter from "./users/users.routes";
import RoleRouter from "./role/role.router"
import categoryRouter from "./category/category.router";
import wordRouter from "./word/word.router";
import notificationRouter from "./notification/notifications.routes";
import commentRouter from "./comments/comment.routes";
import tournamentRouter from "./tournaments/tournament.router"
import  leaderboardRouter from "./leaderBoard/leaderboard.router"
import dashboardRouter from "./dashboard/dashboard.routes"

export default {
  authRouter,
  userRouter,
  RoleRouter,
  categoryRouter,
  wordRouter,
  notificationRouter,
  commentRouter,
  tournamentRouter,
  leaderboardRouter,
  dashboardRouter,
};
