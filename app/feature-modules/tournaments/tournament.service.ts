import {
  FilterQuery,
  PipelineStage,
  Schema,
  Types,
  UpdateQuery,
} from "mongoose";
import { months } from "../../utility/constants/constants";
import { generatePipeline } from "../../utility/pipeline";
import giftsRepo from "./tournament.repo";
import categoryRepo from "./tournament.repo";
import { TOURNAMENT_RESPONSE } from "./tournament.response";
import { ITournament } from "./tournament.types";
import wordService from "../word/word.service";
import tournamentRepo from "./tournament.repo";
import usersServices from "../users/users.services";
import { Roles } from "../role/role.type";
import leaderboardService from "../leaderBoard/leaderboard.service";
import { LEADERBOARD_RESPONSE } from "../leaderBoard/leaderboard.response";
import { INotification } from "../notification/notification.types";
import notificationService from "../notification/notification.service";

const createTournament = (tour: ITournament) =>
  categoryRepo.createTournament(tour);

const joinTournament = async (createrId: string, tourID: string) => {
  const word = await wordService.findOneWord({
    createrId: new Types.ObjectId(createrId),
  });
  //join only approved tournaments
  const tour = await findOneTournament({
    _id: new Types.ObjectId(tourID),
    status: "approve",
  });

  console.log(word, "word");
  console.log(tour, "tour");
  const noOfCreatedWords = word.length;
  const remainingWordsLen = tour[0].limit - noOfCreatedWords;
  const randomWords = await wordService.aggregation([
    // match the words which are not created by creater and find the words according to the tournament level only
    {
      $match: {
        createrId: { $ne: tour[0].createrId },
        level: tour[0].level,
        //ask to sir about this
        category: tour[0].categories,
      },
    },
    { $sample: { size: remainingWordsLen } }, // randomly select words
  ]);
  console.log(randomWords, "randomWords");
  return { word, randomWords };
};

// const generateResultOnSubmit=async (mistakes: number, timeInSeconds:number,totalPoints:number,playerId:string,tourID:string) => {
// // return {
// //   time: timeInSeconds,
// //   points: totalPoints,
// //   mistakes: mistakes,
// //   playerId: new Schema.Types.ObjectId(playerId),
// //   tourID: new Schema.Types.ObjectId(tourID),
// // };
// //update leaderboard automatically

// }

//ask sir about this
const generateResultOnSubmit = async (
  mistakes: number,
  timeInSeconds: number,
  totalPoints: number,
  playerId: string,
  tourID: string
) => {
  console.log(tourID, "tournamentId");
  const tournament = await findOneTournament({ _id: tourID });
  console.log(tournament[0], "tournament");
  if (!tournament[0]) {
    throw new Error("Tournament not found");
  }
  const playerInfo = {
    playerId: playerId,
    noOfMistakes: mistakes,
    timeInSeconds,
    points: totalPoints,
  };
  console.log(playerInfo, "playerInfo");
  const leaderboard = await leaderboardService.findOneLeaderBoard({
    tourID: tourID,
  });
  console.log(leaderboard, "leaderboard");
  if (leaderboard[0]) {
    console.log(leaderboard[0].playerInfo, "playerinfo");
    const playerIndex = leaderboard[0].playerInfo.findIndex(
      (p) => p.playerId.toString() === playerId
    );
    console.log(playerIndex, "playerindex");
    if (playerIndex !== -1) {
      leaderboard[0].playerInfo[playerIndex].noOfMistakes = mistakes;
      leaderboard[0].playerInfo[playerIndex].timeInSeconds = timeInSeconds;
      leaderboard[0].playerInfo[playerIndex].points = totalPoints;
    } else {
      console.log(leaderboard[0].playerInfo, "playerInfo leaderboard");
      leaderboard[0].playerInfo.push(playerInfo);
    }
    return await leaderboardService.updateLeaderBoard(
      { tourID: new Types.ObjectId(tourID) },
      {
        $set: { playerInfo: leaderboard[0].playerInfo },
      }
    );
  } else {
    // Add new player info if player doesn't exist in leaderboard
    const newLeaderboard = {
      tourID: tourID,
      playerInfo: [playerInfo],
    };
    //@ts-ignore
    return await leaderboardService.createLeaderBoard(newLeaderboard);
  }
};

// if (leaderboard[0]) {
//     // Update player info if player already exists in leaderboard
//     const playerIndex = leaderboard[0].playerInfo.findIndex((p) => p.playerId === playerId);
//     if (playerIndex !== -1) {
//       leaderboard[0].playerInfo[playerIndex] = playerInfo;
//     } else {
//       leaderboard[0].playerInfo.push(playerInfo);
//     }
//     await leaderboardService.updateLeaderBoard(filter, { $set: { playerInfo: leaderboard[0].playerInfo } });
//   } else {
//     // Add new player info if player doesn't exist in leaderboard
//     const newLeaderboard = {
//       tourID: tourID,
//       playerInfo: [playerInfo],
//     };
//     await leaderboardService.createLeaderBoard(newLeaderboard);
//   }
// };

const stopTournament = async (tourID: string) => {
  // const leaderBoard = await leaderboardService.findOneLeaderBoard({
  //   tourID: new Types.ObjectId(tourID),
  // });
  // console.log(leaderBoard,"leaderboard")
  const updateStatus = await updateTournament(
    { _id: new Types.ObjectId(tourID) },
    { status: "pause" }
  );
  const winnerInfo = await leaderboardService.aggregation([
    { $match: { tourID: new Types.ObjectId(tourID) } },
    { $unwind: "$playerInfo" },
    {
      $group: {
        _id: "$_id",
        playerId: { $first: "$playerInfo.playerId" },
        points: { $max: "$playerInfo.points" },
        time: { $max: "$playerInfo.timeInSeconds" },
        mistakes: { $max: "$playerInfo.noOfMistakes" },
      },
    },
    { $sort: { points: -1 } },
    { $limit: 1 },
  ]);
  console.log(winnerInfo, "winnerinfo");
  console.log(winnerInfo.length);
  if (winnerInfo.length > 0) {
    const { playerId, points, time, mistakes } = winnerInfo[0];
    console.log(playerId, "playerId");
    const player = await usersServices.findOneUser({ _id: playerId });

    console.log(player, "player");
    const certificateInfo = {
      playerName: player.name,
      playerEmail: player.email,
      points,
      time,
      mistakes,
      tournamentId: tourID,
    };
    //update wins
    const updateWins = await usersServices.updateUser(
      { _id: playerId },
      { $inc: { noOfWins: 1 } }
    );
    // TODO: Generate and send certificate
    console.log(certificateInfo);
    const result = "congratulations! you won the tournament";
    return { result, certificateInfo };
  }
};

const aggregation = (pipeline: PipelineStage[]) =>
  tournamentRepo.aggregation(pipeline);

const updateTournament = async (
  filter: FilterQuery<ITournament>,
  data: UpdateQuery<ITournament>
) => {
  const result = await tournamentRepo.updateTournament(filter, data);
  if (result.modifiedCount > 0) return TOURNAMENT_RESPONSE.UPDATE_SUCCESS;
  return TOURNAMENT_RESPONSE.NOT_FOUND;
};

// const deleteOneCategory = async (id: string) =>
//   await categoryRepo.deleteCategory(id);

const findOneTournament = async (filter: FilterQuery<ITournament>) =>
  tournamentRepo.findOneTournament(filter);

// const findallCategory = async (query: Object) => {
//   const pipeline = generatePipeline(query);
//   const result = await giftsRepo.findallCategory(pipeline);
//   return result;
// };

// const warnComments = async (commentId: string, reason: string) => {
//   const comment = await findOneComment({
//     _id: new Types.ObjectId(commentId),
//   });
//   console.log(comment, "comment");
//   const tournamentId = comment[0].tournamentId;
//   const receiverId = comment[0].userId;
//   const senderId = Roles.MODERATOR;
//   ///create  a notificatoin

//   const notification: INotification = {
//     senderId: senderId,
//     receiverId: receiverId,
//     tournamentId: tournamentId,
//     Notmessge: `Your comment has been flagged for the following reason: ${reason}`,
//   };
//   // Create notification
//   const createdNotification = await notificationService.createNotification(
//     notification
//   );
//   console.log(createdNotification, "createdNotification");
//   const warnedComment = await updateComment(
//     {
//       _id: new Types.ObjectId(commentId),
//     },
//     { warningFlag: true, warnReason: reason }
//   );
//   console.log(warnedComment, "warnedComment");
//   //delete the comment which is not appropriate
//   return await deleteOneComment(commentId);
// };

const approveTournament = async (tourID: string, Notmessage: string) => {
  console.log(tourID, "tourID");
  const updatedTour = await updateTournament(
    { _id: new Types.ObjectId(tourID) },
    { status: "approved" }
  );
  const notification: INotification = {
    senderId: Roles.ADMIN,
    receiverId: Roles.CREATER,
    tournamentId: tourID,
    Notmessge: Notmessage,
  };
  // Create notification
  const createdNotification = await notificationService.createNotification(
    notification
  );
  return createdNotification;
};

const generateValuesForGraph = async (month: Number) => {
  try {
    const pipeline = [
      // Match all documents that have a 'createdAt' field
      { $match: { createdAt: { $exists: true } } },

      // Filter documents by the month
      {
        $match: {
          $expr: {
            $eq: [{ $month: "$createdAt" }, month],
          },
        },
      },

      // Count the number of documents that matched the pipeline
      { $count: "total" },
    ];
    const result = await aggregation(pipeline);
    return result;
  } catch (error) {
    console.error(error);
  }
};

const generateGraph = async () => {
  let GraphDetails = [];
  for (let month of months) {
    const result = await generateValuesForGraph(month);
    const res = { month, result };
    GraphDetails.push(res);
  }
  return GraphDetails;
};

export default {
  generateValuesForGraph,
  createTournament,
  aggregation,
  joinTournament,
  findOneTournament,
  generateResultOnSubmit,
  updateTournament,
  stopTournament,
  approveTournament,
  generateGraph

  // updateCategory,
  // deleteOneCategory,
  // findOneCategory,
  // findallCategory,
};
