export const TOURNAMENT_RESPONSE = {
  NOT_FOUND: {
    statusCode: 404,
    message: "category not found",
  },
  UNABLE_TO_PROCEED: {
    statusCode: 400,
    message: "unable to  proceed",
  },
  UPDATE_SUCCESS: {
    message: "tournament Updated Successfully",
    statusCode: 200,
  },
};
