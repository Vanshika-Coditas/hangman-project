import { Router } from "express";
import { ResponseHandler } from "../../utility/response-handler";
import { NextFunction, Request, Response } from "express";
import { permit } from "../../utility/middleware/permit";

import { createCategoryValidation } from "./tournament.validations";
import { Roles } from "../role/role.type";
import { Types } from "mongoose";
import tournamentService from "./tournament.service";
import { months } from "../../utility/constants/constants";

const router = Router();

router.post(
  "/",
  // createCategoryValidation,
  // permit([Roles.CREATER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const tourData = req.body;
      console.log(tourData);
      const result = await tournamentService.createTournament(tourData);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.put(
  "/stopTour/:tourID",
  // permit([Roles.ADMIN]), // Only admins can stop tournaments
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { tourID } = req.params;
      const result = await tournamentService.stopTournament(tourID);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.post(
  "/joinTournament/:createrId/:tourID",
  // createCategoryValidation,
  // permit([Roles.CREATER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { createrId, tourID } = req.params;
      console.log(createrId);
      const result = await tournamentService.joinTournament(createrId, tourID);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.patch(
  "/tournament/:tourID",
  // add middleware for authentication and authorization if needed
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const tourID = req.params.tourID;
      const result = await tournamentService.stopTournament(tourID);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get(
  "/generateResult",
  // getCategoryValidation,
  // permit([Roles.ADMIN, Roles.PLAYER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { mistakes, time, points, playerId, tourId } = req.body;
      const result = await tournamentService.generateResultOnSubmit(
        Number(mistakes),
        Number(time),
        Number(points),
        playerId,
        tourId
      );
      console.log("result");
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get("/generateGraph", async (req, res) => {
  try {
    const result = await tournamentService.generateGraph();
    res.json(result);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
});

router.patch(
  "/approveTour/:id",
  // getCategoryValidation,
  // permit([Roles.ADMIN, Roles.PLAYER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const{reason}=req.body
      console.log(id,"id")
      const result = await tournamentService.approveTournament(id, reason);
      console.log("result");
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);




// router.patch(
//   "/:shopId",
//   updateCategoryValidation,
//   permit([Roles.ADMIN]),
//   async (req: Request, res: Response, next: NextFunction) => {
//     try {
//       const shopId = req.params.shopId;
//       const data = req.body;
//       const result = await categoryService.updateCategory(
//         { _id: new Types.ObjectId(shopId) },
//         data
//       );
//       res.send(new ResponseHandler(result));
//     } catch (e) {
//       next(e);
//     }
//   }
// );

// router.delete(
//   "/:id",
//   getCategoryValidation,
//   permit([Roles.ADMIN]),
//   async (req: Request, res: Response, next: NextFunction) => {
//     try {
//       console.log("shop routes deleted method");
//       const { id } = req.params;
//       const result = await categoryService.deleteOneCategory(id);
//       res.send(new ResponseHandler(result));
//     } catch (e) {
//       next(e);
//     }
//   }
// );

// router.get(
//   "/",
//   getCategoryValidation,
//   permit([Roles.ADMIN, Roles.PLAYER]),
//   async (req:Request, res:Response, next:NextFunction) => {
//     try {
//       const query = req.query;
//       const result = await categoryService.findallCategory(query);
//       res.json(result);
//       res.send(new ResponseHandler(result));
//     } catch (e) {
//       next(e);
//     }
//   }
// );

router.get(
  "/findOneTournament/:id",
  // getCategoryValidation,
  permit([Roles.ADMIN, Roles.PLAYER]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const result = await tournamentService.findOneTournament({ _id: id });
      console.log("result");
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router;
