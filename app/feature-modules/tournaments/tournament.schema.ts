import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { ITournament } from "./tournament.types";

const tourSchema = new BaseSchema({
  tourName: {
    type: String,
    required: true,
  },
  createrId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  createrName: {
    type: String,
    required: true,
  },
  level: {
    type: String,
    required: true,
  },
  limit: {
    type: Number,
    required: true,
  },
  words: [{
    type: Schema.Types.ObjectId,
    ref: "word",
    required: true,
  }],
  categories: {
    type: [Schema.Types.ObjectId],
    required: true,
  },
  status:{
    type:String,
    required:false,
    default:"pending"
  }
});

type TourDocument = Document & ITournament;

export const tournamentModel = model<TourDocument>("tournament", tourSchema);
