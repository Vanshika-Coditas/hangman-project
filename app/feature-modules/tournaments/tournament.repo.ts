import { FilterQuery, PipelineStage, Types, UpdateQuery } from "mongoose";
import { ITournament } from "./tournament.types";
import { tournamentModel } from "./tournament.schema";

const createTournament = (tour: ITournament) => tournamentModel.create(tour);

const updateTournament = async (
  filter: FilterQuery<ITournament>,
  data: UpdateQuery<ITournament>
) => {
  const result = await tournamentModel.updateOne(filter, data);
  console.log(result);
  return result;
};

const deleteTournament = async (id: string) =>
  await tournamentModel.updateOne(
    { _id: new Types.ObjectId(id) },
    { isDeleted: true }
  );

const findallTournaments = async (pipeline: any) => {
  try {
    const result = await tournamentModel.aggregate(pipeline);
    return result;
  } catch (error) {
    console.error(error);
    throw new Error("Error finding gifts");
  }
};

const findOneTournament = async (filter: FilterQuery<ITournament>) =>
  tournamentModel.find({ isDeleted: false, status: "approved", ...filter });

const aggregation = (pipeline: PipelineStage[]) =>
  tournamentModel.aggregate(pipeline);

export default {
  findOneTournament,
  findallTournaments,
  deleteTournament,
  updateTournament,
  createTournament,
  aggregation,
};
