import { Schema } from "mongoose";
import { IWord } from "../word/word.types";

export interface ITournament {
  tourName: string;
  createrId:string;
  createrName:string;
  level:string;
  limit:number;
  categories:[string];
  status:string;
  words:IWord[]
}



