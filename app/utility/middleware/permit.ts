import { Request, Response, NextFunction } from "express";

export const permit = (roles: string[]) => {
  return (req: Request, res: Response, next: NextFunction) => {
    try {
      const { role } = res.locals.tokenDecode;
      for (let ele of roles) {
        if (ele === role.toString()) return next();
      }
      return next({ message: "Unauthorised Access", statusCode: 401 });
    } catch (e) {
      next(e);
    }
  };
};
